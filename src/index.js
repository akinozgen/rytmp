import React from "react";
import ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
import { initializeFirstLaunch, isFirstLaunch } from "./helpers";
import App from "./components/app";
import './index.css';

if (isFirstLaunch()) {
  initializeFirstLaunch();
}

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
