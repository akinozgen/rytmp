import React, { Component } from "react";
import { capitalize } from "../../helpers";
import PropTypes from "prop-types";

export default class QueueItem extends Component {
  static propTypes = {
    song: PropTypes.object,
    playSelectedSong: PropTypes.func,
    removeFromQueue: PropTypes.func,
    queuePosition: PropTypes.number,
    id: PropTypes.number
  };

  removeFromQueue() {
    this.props.removeFromQueue(this.props.song);
  }

  playSelectedSong() {
    this.props.playSelectedSong(this.props.song);
  }

  render() {
    return (
      <div
        title="Hold to move"
        className={`queue-item ${
          this.props.id === this.props.queuePosition ? "active" : null
        }`}
      >
        <p className="queue-item-title">
          {String(capitalize(this.props.song.snippet.title)).slice(0, 35)}...
        </p>
        <span className="duration">
          {this.props.song.snippet.duration
            ? this.props.song.snippet.duration.split("PT")[1].split("M")[0]
            : "03"}:
          {this.props.song.snippet.duration
            ? this.props.song.snippet.duration
                .split("PT")[1]
                .split("M")[1]
                .split("S")[0]
            : "00"}
        </span>
        <button
          title="Remove From Queue"
          className="remove"
          onClick={this.removeFromQueue.bind(this)}
        >
          <span className="zmdi zmdi-close" />
        </button>
      </div>
    );
  }
}
