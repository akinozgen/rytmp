import React, { Component } from "react";
import PropTypes from "prop-types";
import QueueItem from "./item";
import Reorder from "react-reorder";
import "./index.css";

export default class QueueManagerComponent extends Component {
  static propTypes = {
    queue: PropTypes.array,
    playSelectedSong: PropTypes.func,
    removeFromQueue: PropTypes.func,
    queuePosition: PropTypes.number,
    saveAsPlaylist: PropTypes.func,
    clearQueue: PropTypes.func,
    updateQueue: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.reorder = this.reorder.bind(this);
  }

  reorder($event, movedItem, oldIndexOfItem, newIndexOfItem, reorderedQueue) {
    this.props.updateQueue(reorderedQueue);
  }

  render() {
    return (
      <section className="queue-manager">
        <h3 className="title">
          <span>Queue Manager</span>
          <button
            title="Save as Playlist"
            onClick={this.props.saveAsPlaylist}
            disabled={this.props.queue.length === 0}
          >
            <span className="zmdi zmdi-playlist-plus" />
          </button>
          <button title="Clear Queue" onClick={this.props.clearQueue}>
            <span className="zmdi zmdi-close" />
          </button>
        </h3>
        <div className="queue-items">
          <Reorder
            itemKey="etag"
            lock="horizontal"
            holdTime={500}
            list={this.props.queue}
            callback={this.reorder}
            template={item => (
              <QueueItem
                id={item.item.id.videoId}
                song={item.item}
                playSelectedSong={this.props.playSelectedSong}
                removeFromQueue={this.props.removeFromQueue}
                queuePosition={this.props.queuePosition}
              />
            )}
            selected={this.props.queuePosition}
            disableReorder={false}
          />
        </div>
      </section>
    );
  }
}
