import React, { Component } from "react";
import PlayerComponent from "../playercomponent";
import SearchComponent from "../searchcomponent";
import QueueManagerComponent from "../queuemanagercomponent";
import PlaylistManagerComponent from "../playlistmanagercomponent";
import tm from "title-marquee";
import "./index.css";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchQuery: localStorage.getItem("searchQuery")
        ? localStorage.getItem("searchQuery")
        : "",
      searchResults: localStorage.getItem("searchResults")
        ? JSON.parse(localStorage.getItem("searchResults"))
        : "",
      playlists: [],
      queue: localStorage.getItem("queue")
        ? JSON.parse(localStorage.getItem("queue"))
        : [],
      queuePosition: 0
    };

    this.getSearchResults = this.getSearchResults.bind(this);
    this.getSearchQuery = this.getSearchQuery.bind(this);
    this.playSelectedSong = this.playSelectedSong.bind(this);
    this.addToQueue = this.addToQueue.bind(this);
    this.loadPlaylist = this.loadPlaylist.bind(this);
    this.removePlaylist = this.removePlaylist.bind(this);
    this.removeFromQueue = this.removeFromQueue.bind(this);
    this.setQueuePosition = this.setQueuePosition.bind(this);
    this.saveAsPlaylist = this.saveAsPlaylist.bind(this);
    this.clearQueue = this.clearQueue.bind(this);
    this.getPlaylists = this.getPlaylists.bind(this);
    this.updateQueue = this.updateQueue.bind(this);
    this.removeSongFromPlaylist = this.removeSongFromPlaylist.bind(this);
    this.renamePlaylist = this.renamePlaylist.bind(this);
  }

  componentDidMount() {
    this.getPlaylists();
  }

  componentWillUpdate(nextProps, nextState) {
    localStorage.setItem("queue", JSON.stringify(nextState.queue));
  }

  getPlaylists() {
    let data = Object.keys(localStorage);
    data = data.filter(
      x =>
        !(["length", "queue", "searchQuery", "searchResults"].indexOf(x) > -1)
    );
    data = data.map(i => {
      let ret = `{
        "title": "${i}",
        "videos": ${localStorage.getItem(i)}
      }`;
      return JSON.parse(ret);
    });

    this.setState({ playlists: data });
  }

  getSearchQuery(searchQuery) {
    this.setState({ searchQuery });
    localStorage.setItem("searchQuery", searchQuery);
  }

  getSearchResults(searchResults) {
    this.setState({ searchResults });
    localStorage.setItem("searchResults", JSON.stringify(searchResults));
  }

  playSelectedSong(song) {
    if (!this.state.queue.includes(song)) {
      this.setState({ queue: this.state.queue.concat(song) });
    }
    const newPosition = this.state.queue.includes(song)
      ? this.state.queue.indexOf(song)
      : this.state.queue.length;

    setTimeout(() => {
      this.setState({
        queuePosition: newPosition
      });
      tm({
        text: this.state.queue[newPosition].snippet.title,
        speed: 200,
        buffer: 20
      });
    }, 300);
  }

  removeFromQueue(song) {
    this.setState({
      queue: this.state.queue.filter(x => x.id.videoId !== song.id.videoId)
    });
  }

  addToQueue(song) {
    localStorage.setItem(
      "queue",
      JSON.stringify(this.state.queue.concat(song))
    );
    this.setState({ queue: this.state.queue.concat(song) });
  }

  loadPlaylist(playlist) {
    this.setState({ queue: playlist.videos });
  }

  removePlaylist(playlist) {
    localStorage.removeItem(playlist.title);
    this.getPlaylists();
  }

  setQueuePosition(newPosition) {
    this.setState({ queuePosition: newPosition });
  }

  clearQueue() {
    this.setState({ queue: [] });
  }

  saveAsPlaylist() {
    const playlistName = prompt("What is your playlist name?");

    if (playlistName !== "" && playlistName !== null) {
      localStorage.setItem(playlistName, JSON.stringify(this.state.queue));
      this.getPlaylists();
    } else if (playlistName === "" && playlistName !== null)
      this.saveAsPlaylist();
    else return;
  }

  updateQueue(queue) {
    this.setState({ queue });
  }

  removeSongFromPlaylist(playlist, song) {
    const newPlaylist = playlist.videos.filter(
      playlistSong => playlistSong !== song
    );
    localStorage.setItem(playlist.title, JSON.stringify(newPlaylist));

    this.setState({
      playlists: this.state.playlists.map(pla => {
        if (pla.title === playlist.title) {
          pla.videos = newPlaylist;
        }
        return pla;
      })
    });
  }

  renamePlaylist(playlist) {
    const playlistName = prompt("What is your new playlist name?");

    if (playlistName !== "" && playlistName !== null) {
      localStorage.removeItem(playlist.title);
      localStorage.setItem(playlistName, JSON.stringify(playlist.videos));

      this.setState({
        playlists: this.state.playlists
          .filter(x => x.title !== playlist.title)
          .concat({
            title: playlistName,
            videos: playlist.videos
          })
      });
    } else if (playlistName === "" && playlistName !== null)
      this.renamePlaylist();
    else return;
  }

  render() {
    return (
      <section className="app">
        <div className="grid">
          <div className="left flex-3 bg-gradient-red">
            <div className="grid vertical">
              <div className="top flex-3">
                <PlayerComponent
                  queuePosition={this.state.queuePosition}
                  queue={this.state.queue}
                  setQueuePosition={this.setQueuePosition}
                />
              </div>
              <div className="bottom flex-4">
                <div className="grid bg-gradient-white">
                  <div className="left flex-1 queue-manager-container">
                    <QueueManagerComponent
                      queue={this.state.queue}
                      playSelectedSong={this.playSelectedSong}
                      removeFromQueue={this.removeFromQueue}
                      queuePosition={this.state.queuePosition}
                      clearQueue={this.clearQueue}
                      saveAsPlaylist={this.saveAsPlaylist}
                      updateQueue={this.updateQueue}
                    />
                  </div>
                  <div className="right flex-1 playlist-manager-container">
                    <PlaylistManagerComponent
                      playlists={this.state.playlists}
                      loadPlaylist={this.loadPlaylist}
                      removePlaylist={this.removePlaylist}
                      refreshPlaylists={this.getPlaylists}
                      removeSong={this.removeSongFromPlaylist}
                      renamePlaylist={this.renamePlaylist}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="right flex-1 bg-gradient-grey">
            <SearchComponent
              playSelectedSong={this.playSelectedSong}
              searchResults={this.state.searchResults}
              searchQuery={this.state.searchQuery}
              setSearchResults={this.getSearchResults}
              setSearchQuery={this.getSearchQuery}
              addToQueue={this.addToQueue}
            />
          </div>
        </div>
      </section>
    );
  }
}
