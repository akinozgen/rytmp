import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.css";
import PlaylistItem from "./item";

export default class PlaylistManagerComponent extends Component {
  static propTypes = {
    playlists: PropTypes.array,
    loadPlaylist: PropTypes.func,
    removePlaylist: PropTypes.func,
    refreshPlaylists: PropTypes.func,
    removeSong: PropTypes.func,
    renamePlaylist: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.importPlaylist = this.importPlaylist.bind(this);
  }

  importPlaylist() {
    const fileSelect = document.createElement("input");
    fileSelect.setAttribute("type", "file");
    fileSelect.setAttribute("multiselect", false);
    fileSelect.click();

    fileSelect.oninput = $event => {
      const nameArray = $event.target.files[0].name.split(".");
      if ($event.target.files.length === 0) {
        alert("No file selected!");
        return;
      }

      if (nameArray[nameArray.length - 1] !== "playlist") {
        alert("You must select playlist file (etc. name.playlist)!");
        return;
      }

      const fileReader = new FileReader();
      fileReader.readAsText($event.target.files[0]);

      fileReader.onloadend = $onLoadEndEvent => {
        const playlistContent = JSON.parse($onLoadEndEvent.target.result);

        if (!playlistContent.title || !playlistContent.videos) {
          alert(
            "Wrong playlist format! You can't import outer playlists from this app."
          );
          return;
        }

        localStorage.setItem(
          playlistContent.title,
          JSON.stringify(playlistContent.videos)
        );
        this.props.refreshPlaylists();
      };

      fileReader.onerror = $onErrorEvent => {
        console.log($onErrorEvent);
      };
    };
  }

  render() {
    return (
      <section className="playlist-manager">
        <h3 className="title">
          <span>Playlist Manager</span>
          <button onClick={this.importPlaylist} title="Import Playlist">
            <span className="zmdi zmdi-upload" />
          </button>
        </h3>
        <div className="playlists">
          {this.props.playlists.map((playlist, index) => (
            <PlaylistItem
              key={index + 1}
              playlist={playlist}
              loadPlaylist={this.props.loadPlaylist}
              removePlaylist={this.props.removePlaylist}
              removeSong={this.props.removeSong}
              renamePlaylist={this.props.renamePlaylist}
            />
          ))}
        </div>
      </section>
    );
  }
}
