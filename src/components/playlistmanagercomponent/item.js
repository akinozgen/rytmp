import React, { Component } from "react";
import { capitalize } from "../../helpers";
import PropTypes from "prop-types";

export default class PlaylistItem extends Component {
  static propTypes = {
    playlist: PropTypes.object,
    loadPlaylist: PropTypes.func,
    removePlaylist: PropTypes.func,
    removeSong: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      expanded: false
    };

    this.loadPlaylist = this.loadPlaylist.bind(this);
    this.removePlaylist = this.removePlaylist.bind(this);
    this.exportPlaylist = this.exportPlaylist.bind(this);
    this.removeSong = this.removeSong.bind(this);
    this.renamePlaylist = this.renamePlaylist.bind(this);
  }

  loadPlaylist() {
    this.props.loadPlaylist(this.props.playlist);
  }

  removePlaylist() {
    this.props.removePlaylist(this.props.playlist);
  }

  exportPlaylist() {
    const data = `data:text/json;charset=utf-8,${encodeURIComponent(
      JSON.stringify(this.props.playlist)
    )}`;
    // const anchor = document.createElement("a");
    // anchor.setAttribute("href", data);
    // anchor.setAttribute("download", `${this.props.playlist.title}.playlist`);
    // anchor.click();
    window.open(data);
  }

  removeSong(song) {
    this.props.removeSong(this.props.playlist, song);
  }

  renamePlaylist() {
    this.props.renamePlaylist(this.props.playlist);
  }

  render() {
    return (
      <div className="playlist-item">
        <button
          title="Load Playlist"
          className="play"
          onClick={this.loadPlaylist}
        >
          <span className="zmdi zmdi-play" />
        </button>
        <p className="playlist-title">
          {capitalize(this.props.playlist.title)}
        </p>
        <span className="song-count">
          {this.props.playlist.videos.length} songs
        </span>
        <button
          title="Export playlist"
          className="export"
          onClick={this.exportPlaylist}
        >
          <span className="zmdi zmdi-download" />
        </button>

        <button title="Rename Playlist" onClick={this.renamePlaylist}>
          <span className="zmdi zmdi-format-italic" />
        </button>
        <button
          title="Remove Playlist"
          className="remove"
          onClick={this.removePlaylist}
        >
          <span className="zmdi zmdi-close" />
        </button>
        <button
          className="expand"
          title="Expand Playlist Content"
          onClick={() => this.setState({ expanded: !this.state.expanded })}
        >
          <span
            className={`zmdi zmdi-chevron-down ${
              this.state.expanded ? "rotate" : null
            }`}
          />
        </button>
        <div
          className={`playlist-content ${
            this.state.expanded ? "expanded" : null
          }`}
        >
          <ul>
            {this.props.playlist.videos.map((song, index) => (
              <li className="playlist-content-title" key={index + 1}>
                <span>
                  {String(capitalize(song.snippet.title)).slice(0, 60)}...
                </span>
                <button
                  title="Remove Track"
                  onClick={() => this.removeSong(song)}
                >
                  <span className="zmdi zmdi-close" />
                </button>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}
