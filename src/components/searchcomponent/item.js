import React, { Component } from "react";
import PropTypes from "prop-types";
import { capitalize } from "../../helpers";
import "./index.css";
import "react-simple-dropdown/styles/Dropdown.css";

export default class SearchResultItem extends Component {
  static propTypes = {
    song: PropTypes.object,
    play: PropTypes.func,
    addToQueue: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      expanded: false
    };

    this.addToQueue = this.addToQueue.bind(this);
    this.play = this.play.bind(this);
  }

  addToQueue() {
    this.props.addToQueue(this.props.song);
    this.setState({ expanded: !this.state.expanded });
  }

  play() {
    this.props.play(this.props.song);
  }

  render() {
    return (
      <div className="search-result-item">
        <button title="Play" className="play" onClick={this.play}>
          <span className="zmdi zmdi-play" />
        </button>
        <p title={capitalize(this.props.song.snippet.title)}>
          {String(capitalize(this.props.song.snippet.title)).slice(0, 23) +
            "..."}
        </p>
        <button
          title="More Options"
          className="more"
          onClick={() => this.setState({ expanded: !this.state.expanded })}
        >
          <span className="zmdi zmdi-more-vert" />
        </button>
        <div className={`actions ${this.state.expanded ? "expanded" : null}`}>
          <button title="Add to Queue" onClick={this.addToQueue}>
            <span className="zmdi zmdi-playlist-plus" />
            <span>Add To Queue</span>
          </button>
        </div>
      </div>
    );
  }
}
