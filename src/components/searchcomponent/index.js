import React, { Component } from "react";
import YTSearch from "youtube-search-api-with-axios";
import SearchResultItem from "./item";
import "./index.css";
import PropTypes from "prop-types";

export default class SearchComponent extends Component {
  static propTypes = {
    searchResults: PropTypes.array,
    setSearchResults: PropTypes.func,
    searchQuery: PropTypes.string,
    setSearchQuery: PropTypes.func,
    playSelectedSong: PropTypes.func,
    addToQueue: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {};

    this.onInput = this.onInput.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.play = this.play.bind(this);
    this.addToQueue = this.addToQueue.bind(this);
  }

  onInput($event) {
    this.props.setSearchQuery($event.target.value);
  }

  onKeyDown($event) {
    if ($event.keyCode && $event.keyCode === 13) {
      this.search();
    }
  }

  search() {
    YTSearch(
      {
        key: "AIzaSyD7GI67U8jOax0uBV301aF9XnJIA9XJfKE",
        q: this.props.searchQuery,
        maxResults: 20
      },
      videos => {
        this.props.setSearchResults(videos);
        this.getDurations(videos);
      }
    );
  }

  async getDurations(videos) {
    for (let i = 0; i < videos.length; i++) {
      const response = await fetch(
        `https://www.googleapis.com/youtube/v3/videos?id=${
          videos[i].id.videoId
        }&part=contentDetails&key=AIzaSyD7GI67U8jOax0uBV301aF9XnJIA9XJfKE`
      );

      const result = await response.json();
      const duration = result.items[0].contentDetails.duration;

      videos[i].snippet.duration = duration;
    }

    this.props.setSearchResults(videos);
  }

  play(song) {
    this.props.playSelectedSong(song);
  }

  addToQueue(song) {
    this.props.addToQueue(song);
  }

  render() {
    return (
      <section className="search">
        <div className="input">
          <input
            type="text"
            placeholder="Search..."
            defaultValue={this.props.searchQuery}
            onInput={this.onInput}
            onKeyDown={this.onKeyDown}
          />
          <span className="zmdi zmdi-search" />
        </div>
        <ul>
          {this.props.searchResults.map((resultItem, index) => (
            <li key={index + 1}>
              <SearchResultItem
                song={resultItem}
                play={this.play}
                addToQueue={this.addToQueue}
              />
            </li>
          ))}
        </ul>
      </section>
    );
  }
}
