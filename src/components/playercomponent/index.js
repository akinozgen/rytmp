import React, { Component } from "react";
import PropTypes from "prop-types";
import Audio from "react-audioplayer";
import "./index.css";

export default class PlayerComponent extends Component {
  static propTypes = {
    queuePosition: PropTypes.number,
    queue: PropTypes.array,
    setQueuePosition: PropTypes.func
  };

  videoElement;
  _mounted = false;

  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.queuePosition !== nextProps.queuePosition && this.audio && this._mounted) {
      const span = this.props.queuePosition - nextProps.queuePosition;

      for (let i = 0; i < span; span < 0 ? i-- : i++) {
        if (span < 0) {
          this.audio.skipToPrevious();
        } else {
          this.audio.skipToNext();
        }
      }

      this.audio.audioElement.play();
    }
  }

  componentDidMount() {
    this._mounted = true;

    const playerBg = document.querySelector(
      ".audioElements__commentsWrapperBackground___3avoO"
    );
    this.videoElement = document.createElement("video");
    this.videoElement.classList.add("player-bg-video");
    this.videoElement.setAttribute("src", "/images/visualizer.mp4");
    this.videoElement.loop = true;
    playerBg.appendChild(this.videoElement);

    this.audio.audioElement.onplaying = $event => {
      const currentSongId = this.props.queue.indexOf(
        this.props.queue.filter(
          x => x.id.videoId === this.audio.audioElement.src.split("=")[2]
        )[0]
      );
      this.props.setQueuePosition(currentSongId);
      this.videoElement.play();
    };

    this.audio.audioElement.onpause = this.audio.audioElement.onstalled = this.audio.audioElement.onended = $event => {
      this.videoElement.pause();
    };
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  render() {
    return (
      <div className="player">
        <Audio
          ref={x => (this.audio = x)}
          width={400}
          height={220}
          fullPlayer={true}
          playlist={
            this.props.queue.length === 0
              ? [
                  {
                    name: "Playlist Empty",
                    src:
                      "https://raw.githubusercontent.com/anars/blank-audio/master/5-seconds-of-silence.mp3",
                    img: "http://placehold.it/200x200?text=Playlist Empty"
                  }
                ]
              : this.props.queue.map(song => {
                  return {
                    name: song.snippet.title,
                    src: `http://www.convertmp3.io/fetch/?video=https://youtube.com/watch?v=${
                      song.id.videoId
                    }`,
                    img: song.snippet.thumbnails.medium.url
                  };
                })
          }
        />
      </div>
    );
  }
}
