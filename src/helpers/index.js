export function isFirstLaunch() {
  return localStorage.length === 0;
}

export function initializeFirstLaunch() {
  localStorage.setItem("queue", JSON.stringify(DEFAULT_QUEUE));
  localStorage.setItem("searchQuery", "Eminem");
  localStorage.setItem("searchResults", JSON.stringify(DEFAULT_RESULTS));
  localStorage.setItem("My first playlist", JSON.stringify(DEFAULT_QUEUE));
}

export function capitalize(str) {
  return str.replace(/\w\S*/g, function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

export const DEFAULT_QUEUE = [
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/fOatISocUPwQQB9jRbLVQNleHB0"',
    id: { kind: "youtube#video", videoId: "_Yhyp-_hX2s" },
    snippet: {
      publishedAt: "2015-08-07T01:12:12.000Z",
      channelId: "UCstaTFTqZAC_OqfAq_JF6vA",
      title: "Eminem - Lose Yourself [HD]",
      description:
        "feat. Eminem from the movie 8 MILE No copyright infringement intended. All contents belong to its rightful owners. This is for entertainment purposes only.",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/_Yhyp-_hX2s/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/_Yhyp-_hX2s/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/_Yhyp-_hX2s/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "msvogue23",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/sRrGlkoN36m9rzfkYS7Uz3luJbw"',
    id: { kind: "youtube#video", videoId: "wfWIs2gFTAM" },
    snippet: {
      publishedAt: "2018-02-14T15:00:06.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - River ft. Ed Sheeran",
      description:
        'The official video for Eminem\'s "River" featuring Ed Sheeran. Available now on the album Revival: http://shady.sr/Revival For more visit: http://eminem.com ...',
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/wfWIs2gFTAM/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/wfWIs2gFTAM/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/wfWIs2gFTAM/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/OpHP7xf5GBcMVFP1L7Lg_lHQSOo"',
    id: { kind: "youtube#video", videoId: "PVbQrvlB_gw" },
    snippet: {
      publishedAt: "2018-04-03T16:00:04.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Framed",
      description:
        'The official video for Eminem\'s "Framed" off the album Revival. Available now: http://shady.sr/Revival For more visit: http://eminem.com ...',
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/PVbQrvlB_gw/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/PVbQrvlB_gw/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/PVbQrvlB_gw/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/462Wzguy8LdnW8cQzKS3P9yPKMo"',
    id: { kind: "youtube#video", videoId: "XbGs_qK2PQA" },
    snippet: {
      publishedAt: "2013-11-27T16:50:00.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Rap God (Explicit)",
      description:
        "Download Eminem's 'MMLP2' Album on iTunes now:http://smarturl.it/MMLP2 Credits below Video Director: Rich Lee Video Producer: Justin Diener Video ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/XbGs_qK2PQA/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/XbGs_qK2PQA/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/XbGs_qK2PQA/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/ZLOj_eulBGiDxDc_f0UcpUZun_8"',
    id: { kind: "youtube#video", videoId: "dARAN1z2KqY" },
    snippet: {
      publishedAt: "2018-05-03T15:59:55.000Z",
      channelId: "UC5GHh0ZgZvBIzjauuVyu71w",
      title: "Royce da 5'9\" - Caterpillar ft. Eminem, King Green",
      description:
        "Music video by Royce 5'9\" and Eminem performing Caterpillar Get the new Royce 5'9 album 'Book of Ryan' - http://smarturl.it/BookofRyan Follow Royce 5'9\" ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/dARAN1z2KqY/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/dARAN1z2KqY/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/dARAN1z2KqY/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "RoyceDa59VEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/FTtKSwtGoHXxlCvWG5OF3Ntkrtg"',
    id: { kind: "youtube#video", videoId: "j5-yKhDd64s" },
    snippet: {
      publishedAt: "2010-06-05T05:02:39.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Not Afraid",
      description:
        "Music video by Eminem performing Not Afraid. (C) 2010 Aftermath Records #VEVOCertified on September 11, 2010.http://www.vevo.com/certified ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/j5-yKhDd64s/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/j5-yKhDd64s/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/j5-yKhDd64s/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/tHOsoG6XcoVhmGLlhKr11HwcfGU"',
    id: { kind: "youtube#video", videoId: "YVkUvmDQ3HY" },
    snippet: {
      publishedAt: "2009-06-16T23:00:29.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Without Me",
      description:
        "iTunes: http://smarturl.it/WithoutMe Amazon: http://smarturl.it/WithoutMeAmz Google Play: http://smarturl.it/WithoutMeGP Playlist Best of Eminem: ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/YVkUvmDQ3HY/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/YVkUvmDQ3HY/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/YVkUvmDQ3HY/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/FPVvTLVggKbXD1aDU2hTCbmuOb0"',
    id: { kind: "youtube#video", videoId: "FMQD9fU4ptA" },
    snippet: {
      publishedAt: "2018-06-05T08:00:25.000Z",
      channelId: "UCJxHzJ97iOYhSTOmiE7Y69w",
      title:
        'Nicki Minaj Responds Eminem: "If he says we together, then we together"',
      description:
        "Eminem & Shady Records News: http://www.southpawer.com/ Like Southpaw on facebook:: https://www.facebook.com/Southpawer/ Follow us on twitter:: ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/FMQD9fU4ptA/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/FMQD9fU4ptA/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/FMQD9fU4ptA/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "Southpawer Kenny",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/KONisaQLrHE8yybb-zQ8GVHFENs"',
    id: { kind: "youtube#video", videoId: "owQ8es-65OM" },
    snippet: {
      publishedAt: "2018-06-05T16:16:46.000Z",
      channelId: "UCI6CO_GQeyBZra-PxldL7sw",
      title: "EMINEM FT OBIE TRIC & 50 CENT - LOVE ME",
      description:
        "TWITTER: https://twitter.com/Preston_KTF I DO NOT OWN ANY COPYRIGHTS TO THIS VIDEO https://w...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/owQ8es-65OM/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/owQ8es-65OM/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/owQ8es-65OM/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "KTF Reactions",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/KONisaQLrHE8yybb-zQ8GVHFENs"',
    id: { kind: "youtube#video", videoId: "owQ8es-65OM" },
    snippet: {
      publishedAt: "2018-06-05T16:16:46.000Z",
      channelId: "UCI6CO_GQeyBZra-PxldL7sw",
      title: "EMINEM FT OBIE TRIC & 50 CENT - LOVE ME",
      description:
        "TWITTER: https://twitter.com/Preston_KTF I DO NOT OWN ANY COPYRIGHTS TO THIS VIDEO https://w...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/owQ8es-65OM/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/owQ8es-65OM/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/owQ8es-65OM/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "KTF Reactions",
      duration: "PT3M52S"
    }
  }
];

export const DEFAULT_RESULTS = [
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/fOatISocUPwQQB9jRbLVQNleHB0"',
    id: { kind: "youtube#video", videoId: "_Yhyp-_hX2s" },
    snippet: {
      publishedAt: "2015-08-07T01:12:12.000Z",
      channelId: "UCstaTFTqZAC_OqfAq_JF6vA",
      title: "Eminem - Lose Yourself [HD]",
      description:
        "feat. Eminem from the movie 8 MILE No copyright infringement intended. All contents belong to its rightful owners. This is for entertainment purposes only.",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/_Yhyp-_hX2s/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/_Yhyp-_hX2s/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/_Yhyp-_hX2s/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "msvogue23",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/sRrGlkoN36m9rzfkYS7Uz3luJbw"',
    id: { kind: "youtube#video", videoId: "wfWIs2gFTAM" },
    snippet: {
      publishedAt: "2018-02-14T15:00:06.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - River ft. Ed Sheeran",
      description:
        'The official video for Eminem\'s "River" featuring Ed Sheeran. Available now on the album Revival: http://shady.sr/Revival For more visit: http://eminem.com ...',
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/wfWIs2gFTAM/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/wfWIs2gFTAM/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/wfWIs2gFTAM/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/OpHP7xf5GBcMVFP1L7Lg_lHQSOo"',
    id: { kind: "youtube#video", videoId: "PVbQrvlB_gw" },
    snippet: {
      publishedAt: "2018-04-03T16:00:04.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Framed",
      description:
        'The official video for Eminem\'s "Framed" off the album Revival. Available now: http://shady.sr/Revival For more visit: http://eminem.com ...',
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/PVbQrvlB_gw/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/PVbQrvlB_gw/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/PVbQrvlB_gw/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/462Wzguy8LdnW8cQzKS3P9yPKMo"',
    id: { kind: "youtube#video", videoId: "XbGs_qK2PQA" },
    snippet: {
      publishedAt: "2013-11-27T16:50:00.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Rap God (Explicit)",
      description:
        "Download Eminem's 'MMLP2' Album on iTunes now:http://smarturl.it/MMLP2 Credits below Video Director: Rich Lee Video Producer: Justin Diener Video ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/XbGs_qK2PQA/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/XbGs_qK2PQA/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/XbGs_qK2PQA/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/ZLOj_eulBGiDxDc_f0UcpUZun_8"',
    id: { kind: "youtube#video", videoId: "dARAN1z2KqY" },
    snippet: {
      publishedAt: "2018-05-03T15:59:55.000Z",
      channelId: "UC5GHh0ZgZvBIzjauuVyu71w",
      title: "Royce da 5'9\" - Caterpillar ft. Eminem, King Green",
      description:
        "Music video by Royce 5'9\" and Eminem performing Caterpillar Get the new Royce 5'9 album 'Book of Ryan' - http://smarturl.it/BookofRyan Follow Royce 5'9\" ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/dARAN1z2KqY/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/dARAN1z2KqY/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/dARAN1z2KqY/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "RoyceDa59VEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/FTtKSwtGoHXxlCvWG5OF3Ntkrtg"',
    id: { kind: "youtube#video", videoId: "j5-yKhDd64s" },
    snippet: {
      publishedAt: "2010-06-05T05:02:39.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Not Afraid",
      description:
        "Music video by Eminem performing Not Afraid. (C) 2010 Aftermath Records #VEVOCertified on September 11, 2010.http://www.vevo.com/certified ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/j5-yKhDd64s/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/j5-yKhDd64s/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/j5-yKhDd64s/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/tHOsoG6XcoVhmGLlhKr11HwcfGU"',
    id: { kind: "youtube#video", videoId: "YVkUvmDQ3HY" },
    snippet: {
      publishedAt: "2009-06-16T23:00:29.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Without Me",
      description:
        "iTunes: http://smarturl.it/WithoutMe Amazon: http://smarturl.it/WithoutMeAmz Google Play: http://smarturl.it/WithoutMeGP Playlist Best of Eminem: ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/YVkUvmDQ3HY/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/YVkUvmDQ3HY/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/YVkUvmDQ3HY/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/FPVvTLVggKbXD1aDU2hTCbmuOb0"',
    id: { kind: "youtube#video", videoId: "FMQD9fU4ptA" },
    snippet: {
      publishedAt: "2018-06-05T08:00:25.000Z",
      channelId: "UCJxHzJ97iOYhSTOmiE7Y69w",
      title:
        'Nicki Minaj Responds Eminem: "If he says we together, then we together"',
      description:
        "Eminem & Shady Records News: http://www.southpawer.com/ Like Southpaw on facebook:: https://www.facebook.com/Southpawer/ Follow us on twitter:: ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/FMQD9fU4ptA/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/FMQD9fU4ptA/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/FMQD9fU4ptA/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "Southpawer Kenny",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/KONisaQLrHE8yybb-zQ8GVHFENs"',
    id: { kind: "youtube#video", videoId: "owQ8es-65OM" },
    snippet: {
      publishedAt: "2018-06-05T16:16:46.000Z",
      channelId: "UCI6CO_GQeyBZra-PxldL7sw",
      title: "EMINEM FT OBIE TRIC & 50 CENT - LOVE ME",
      description:
        "TWITTER: https://twitter.com/Preston_KTF I DO NOT OWN ANY COPYRIGHTS TO THIS VIDEO https://w...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/owQ8es-65OM/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/owQ8es-65OM/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/owQ8es-65OM/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "KTF Reactions",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/b0LrklDzwoW7-kI1rcUTsY2-Lc4"',
    id: { kind: "youtube#video", videoId: "XL5PyejfpRQ" },
    snippet: {
      publishedAt: "2018-06-04T17:31:43.000Z",
      channelId: "UCFAiDfCCRXV2megBznfKijQ",
      title: 'Eminem - "Warrior" ft. Tupac (Hardcore 2018)',
      description:
        "Fitness Gym Motivational Rap HipHop Music - Warrior Remix By: Shady Mixes SoundCloud: https://soundcloud.com/user-249940654 Youtube: ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/XL5PyejfpRQ/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/XL5PyejfpRQ/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/XL5PyejfpRQ/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "Shady Mixes",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/zgMfU8aDoYcMb0nawRtC3iAQxOQ"',
    id: { kind: "youtube#video", videoId: "TLEiLtdSF2M" },
    snippet: {
      publishedAt: "2018-06-05T07:26:26.000Z",
      channelId: "UCJxHzJ97iOYhSTOmiE7Y69w",
      title:
        'Eminem brings out PHresher for "Chloraseptic (Remix)" in NYC (Full, Best Quality, 2018)',
      description:
        "Eminem & Shady Records News: http://www.southpawer.com/ Like Southpaw on facebook:: https://www.facebook.com/Southpawer/ Follow us on twitter:: ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/TLEiLtdSF2M/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/TLEiLtdSF2M/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/TLEiLtdSF2M/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "Southpawer Kenny",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/6PTQwBsG7H13vd_TZCTH9RnkMOU"',
    id: { kind: "youtube#video", videoId: "3BXDsVD6O10" },
    snippet: {
      publishedAt: "2017-12-15T05:00:00.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - River (Audio) ft. Ed Sheeran",
      description:
        "Eminem's track \"River\" ft. Ed Sheeran is available on the album 'Revival,' out now: http://shady.sr/Revival For more visit: http://eminem.com ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/3BXDsVD6O10/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/3BXDsVD6O10/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/3BXDsVD6O10/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/wA0dCCAfTZms52r67scXbhJY4fM"',
    id: { kind: "youtube#video", videoId: "0uMdkFwk9EU" },
    snippet: {
      publishedAt: "2018-06-05T11:59:14.000Z",
      channelId: "UCJxHzJ97iOYhSTOmiE7Y69w",
      title:
        'Fans sing "Stan" along Skylar Grey & Eminem in NYC (Governors Ball 2018)',
      description:
        "Eminem & Shady Records News: http://www.southpawer.com/ Like Southpaw on facebook:: https://www.facebook.com/Southpawer/ Follow us on twitter:: ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/0uMdkFwk9EU/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/0uMdkFwk9EU/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/0uMdkFwk9EU/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "Southpawer Kenny",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/-mfTD47LuZdYwnv0VXyMsIojUBM"',
    id: { kind: "youtube#video", videoId: "ryr75N0nki0" },
    snippet: {
      publishedAt: "2017-12-24T03:30:08.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Walk On Water (Official Video)",
      description:
        'The official video for Eminem\'s "Walk On Water" featuring Beyoncé. Available now on the album Revival: http://shady.sr/Revival For more visit: http://eminem.com ...',
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/ryr75N0nki0/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/ryr75N0nki0/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/ryr75N0nki0/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/uQdJOCzSLtu8YxwhpVtfLbDPqFM"',
    id: { kind: "youtube#video", videoId: "1wYNFfgrXTI" },
    snippet: {
      publishedAt: "2009-06-17T00:23:36.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - When I'm Gone",
      description:
        "Playlist Best of Eminem: https://goo.gl/AquNpo Subscribe for more: https://goo.gl/DxCrDV Music video by Eminem performing When I'm Gone. (C) 2005 ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/1wYNFfgrXTI/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/1wYNFfgrXTI/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/1wYNFfgrXTI/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/xVb5DTslzESnNCRefD_vzEU8rX8"',
    id: { kind: "youtube#video", videoId: "gOMhN-hfMtY" },
    snippet: {
      publishedAt: "2009-12-25T06:54:41.000Z",
      channelId: "UC20vb-R_px4CguHzzBPhoyQ",
      title: "Eminem - Stan (Long Version) ft. Dido",
      description:
        "Music video by Eminem performing Stan. YouTube view counts pre-VEVO: 3965564. (C) 2002 Aftermath Entertainment/Interscope Records.",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/gOMhN-hfMtY/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/gOMhN-hfMtY/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/gOMhN-hfMtY/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "EminemVEVO",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/v3zsp-EYJinbrgxLgHB9V5DIOcE"',
    id: { kind: "youtube#video", videoId: "ytQ5CYE1VZw" },
    snippet: {
      publishedAt: "2015-09-17T03:22:07.000Z",
      channelId: "UCstaTFTqZAC_OqfAq_JF6vA",
      title: "Eminem - Till I Collapse [HD]",
      description:
        "feat. Hugh Jackman from the movie REAL STEEL No copyright infringement intended. All contents belong to its rightful owners. This is for entertainment ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/ytQ5CYE1VZw/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/ytQ5CYE1VZw/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/ytQ5CYE1VZw/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "msvogue23",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/LmZ_HPHJAzv1SMOnaOBOzruLpi4"',
    id: { kind: "youtube#video", videoId: "Rmzkj7gqGqI" },
    snippet: {
      publishedAt: "2018-06-05T15:30:32.000Z",
      channelId: "UCD-RtEZOOIIe0TrZF14SUJA",
      title: "EMINEM MAKEUP TRANSFORMATION",
      description:
        "Contactez-moi par ici ↓↓↓ ♥ ☼ Alors, déjà, abonne-toi à la chaîne parce que ça te rendra cool et c'est cool d'être cool ☼ LA CHAINE DE KRONOMUZIK ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/Rmzkj7gqGqI/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/Rmzkj7gqGqI/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/Rmzkj7gqGqI/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "LeDahliaRose",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/2xLXM6YLD0DpBLMfDcbLp1dhRK4"',
    id: { kind: "youtube#video", videoId: "730O3r61VIE" },
    snippet: {
      publishedAt: "2018-06-04T13:05:41.000Z",
      channelId: "UCJxHzJ97iOYhSTOmiE7Y69w",
      title: 'Eminem - "River" (Live at Governors Ball 2018)',
      description:
        "Eminem & Shady Records News: http://www.southpawer.com/ Like Southpaw on facebook:: https://www.facebook.com/Southpawer/ Follow us on twitter:: ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/730O3r61VIE/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/730O3r61VIE/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/730O3r61VIE/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "Southpawer Kenny",
      duration: "PT3M52S"
    }
  },
  {
    kind: "youtube#searchResult",
    etag: '"DuHzAJ-eQIiCIp7p4ldoVcVAOeY/KssKe1bM-uhkHBP1uGSTMXMSE1M"',
    id: { kind: "youtube#video", videoId: "sHcmg--ivzE" },
    snippet: {
      publishedAt: "2018-06-04T11:28:58.000Z",
      channelId: "UCJxHzJ97iOYhSTOmiE7Y69w",
      title:
        "Eminem calls Nicki Minaj 'a wifey,' lies down on stage and screams \"Nickiii\" (May 3, 2018)",
      description:
        "Eminem & Shady Records News: http://www.southpawer.com/ Like Southpaw on facebook:: https://www.facebook.com/Southpawer/ Follow us on twitter:: ...",
      thumbnails: {
        default: {
          url: "https://i.ytimg.com/vi/sHcmg--ivzE/default.jpg",
          width: 120,
          height: 90
        },
        medium: {
          url: "https://i.ytimg.com/vi/sHcmg--ivzE/mqdefault.jpg",
          width: 320,
          height: 180
        },
        high: {
          url: "https://i.ytimg.com/vi/sHcmg--ivzE/hqdefault.jpg",
          width: 480,
          height: 360
        }
      },
      channelTitle: "Southpawer Kenny",
      duration: "PT3M52S"
    }
  }
];
